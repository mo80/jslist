﻿/* jslist javascript code
 Author: Mo Harrim - 2016 */
var jsList = function (name) {
    // draw component
    // create form, input, ul, div elements
    this.list = document.querySelector('div[data-list="' + name + '"]');
    this.result = document.createElement("div");
    this.result.setAttribute("data-result", name);
    this.result.setAttribute("display", "none");
    this.form = document.createElement("form");
    this.form.setAttribute("data-form", name);
    this.dataWrapper = document.createElement("div");
    this.dataWrapper.setAttribute("data-wrapper", name);
    this.addWrap = document.createElement("div");
    this.addWrap.setAttribute("data-addWrap", name);
    this.dataText = document.createElement("div");
    this.dataText.setAttribute("data-text", name);
    this.dataButton = document.createElement("div");
    this.dataButton.setAttribute("data-button", name);   
    // create input textbox
    this.input = document.createElement("input");
    this.input.setAttribute("type", "text");
    this.input.setAttribute("data-text", name);
    // add it to datatext div
    this.dataText.appendChild(this.input);
    // create add button
    this.addButton = document.createElement("input");
    this.addButton.setAttribute("type", "button");
    this.addButton.setAttribute("value", "+ Add");
    this.addButton.setAttribute("data-button", name);
    // add it to databutton div
    this.dataButton.appendChild(this.addButton);
    // add datatext div to addwrap div
    this.addWrap.appendChild(this.dataText);
    // add databutton div to addwrap div
    this.addWrap.appendChild(this.dataButton);
    // create ul list
    this.el = document.createElement("ul");
    this.el.setAttribute("data-ul", name);
    // submit button
    this.submitButton = document.createElement("input");
    this.submitButton.setAttribute("type", "submit");
    this.submitButton.setAttribute("value", "Submit");
    this.submitButton.setAttribute("data-submit", name);
    // add result div at top
    //this.dataWrapper.appendChild(this.result);
    // add addwrap to datawrapper div
    this.dataWrapper.appendChild(this.addWrap);
    // add ul list to datawrapper div
    this.dataWrapper.appendChild(this.el);
    this.dataWrapper.appendChild(this.submitButton);
    // add dataWrapper to form
    this.form.appendChild(this.dataWrapper);
    // add dataWrapper (entire element tree) to beginning of form
    this.list.insertBefore(this.form, this.list.firstChild);
    this.list.insertBefore(this.result, this.list.firstChild);
    // array to hold end result
    this.arr = [];
    var that = this;
    // add list item function
    this.addEntry = function (entry) {
        //Create List Item
        var listItem = document.createElement("li");
        //label
        var label = document.createElement("label");
        label.innerText = entry;
        // create delete button
        var deleteButton = document.createElement("button");
        deleteButton.innerText = "\u2716";
        deleteButton.className = "delete";
        //bind deleteTask to delete button
        deleteButton.onclick = that.deleteEntry;
        // append
        listItem.appendChild(label);
        listItem.appendChild(deleteButton);
        // add all to list
        that.el.appendChild(listItem);
        // add to arr
        that.arr.push(entry);
    }
    // delete list item from list
    this.deleteEntry = function () {
        var listItem = this.parentNode;
        var list = listItem.parentNode;
        //Remove the parent list item from the ul
        list.removeChild(listItem);
        // delete from arr
        that.arr.pop(listItem.childNodes[0].innerText)
    }
    // initialize the component with listeners
    this.init = function () {
        this.add();
        this.submit();
    }
}
// add
jsList.prototype.add = function () {
    // textbox enter key hit
    this.input.addEventListener('keyup', function (e) {
        e.preventDefault();
        if (e.keyCode === 13 && e.target.value.length > 0) {
            // call addEntry function to add the value to the list
            this.addEntry(e.target.value);
            // clear text
            e.target.value = null;
        }
    }.bind(this));
    // add button click
    this.addButton.addEventListener('click', function (e) {
        e.preventDefault();
        if (this.input.value.length > 0) {
            // call addEntry function to add the value to the list
            this.addEntry(this.input.value);
            this.input.value = null;
        }
    }.bind(this));
}
// form submit
jsList.prototype.submit = function () {
    // stop the enter key from submitting form since we are using it for items addition
    this.form.addEventListener('keypress', function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    }.bind(this));
    // on submit event
    this.form.addEventListener('submit', function (e) {
        e.preventDefault();
        // check if there are list items added
        if (this.arr.length > 0) {
            this.result.innerText = "Thank you for submitting your list: " + this.arr;
            // remove element once all submitted
            this.dataWrapper.remove();
            this.result.setAttribute("display", "inline-block");
            return true;
        }
    }.bind(this));
    
}