JSList Form Documentation

Author: Mo Harrim - 2016
*************************************************************************
JSList Form

The Form Component allows users to add items to a list all through plain Javascript and CSS
It's reusable, simply follow the easy steps below, it is great for search, keywords, tags, filter, or any other list functionality.

CSS

- Make sure to include the jslist.css CSS file and add css file reference in the head section of your page:


```
#!html

<link type="text/css" rel="stylesheet" href="jslist.css" />
```



The CSS file is pretty straight forward with comments to make it easy and customizable


JS

- Make sure to include the jslist.js JS file and add the entire part below where you want the form to be generated in your page

- Make sure the instance name new jslist('listnamehere') corresponds to the div's data-list="listnamehere":


```
#!html

<div data-list="list">
</div>
<script src="jslist.js"></script>
<script>
    // initialize
    (function () {
        // instance name has to match the value of data-list attribute in the above corresponding div (data-list='list')
        var thislist = new jsList('list').init();
    })();
</script>
```


The jsList Form is easy to use, reusable and customizable/stylable